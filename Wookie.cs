﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionWookies
{
    public class Wookie
    {
        public string Nom { get; set; }
        public DateTime DateNaissance { get; set; }
    }
}
