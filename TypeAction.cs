﻿public enum TypeAction
{
	Naissance = 1,
	Deces,
	ConsultationMorts,
	ConsultationVivants,
	ConsultationTues,
    QuitterProgramme
}